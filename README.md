## aospa_X00TD-userdebug 12 SKQ1.211230.001 eng.kunmun.20220205.012248 test-keys
- Manufacturer: asus
- Platform: sdm660
- Codename: X00TD
- Brand: asus
- Flavor: aospa_X00TD-userdebug
- Release Version: 12
- Id: SKQ1.211230.001
- Incremental: eng.kunmun.20220205.012248
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: asus/aospa_X00TD/X00TD:12/SKQ1.211230.001/kunmun02050120:userdebug/test-keys
- OTA version: 
- Branch: aospa_X00TD-userdebug-12-SKQ1.211230.001-eng.kunmun.20220205.012248-test-keys
- Repo: asus_x00td_dump_13399


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
